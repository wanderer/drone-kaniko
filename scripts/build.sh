#!/bin/sh

# force go modules
export GOPATH=""

# disable cgo
export CGO_ENABLED=0

set -e
set -x

# linux
GOOS=linux GOARCH=amd64 go build -o release/linux/amd64/kaniko-gcr    ./cmd/kaniko-gcr
GOOS=linux GOARCH=amd64 go build -o release/linux/amd64/kaniko-ecr    ./cmd/kaniko-ecr
GOOS=linux GOARCH=amd64 go build -o release/linux/amd64/kaniko-docker ./cmd/kaniko-docker

